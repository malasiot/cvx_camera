#include <iostream>

#include <cvx/util/misc/path.hpp>
#include <cvx/camera/calibration.hpp>
#include <cvx/camera/pose.hpp>
#include <cvx/util/misc/arg_parser.hpp>

#include <ceres/ceres.h>
#include <ceres/rotation.h>

#include <regex>

using namespace std ;
using namespace cvx::util ;
using namespace cvx::camera ;
using namespace Eigen ;

istream &operator >> (istream &strm, cv::Size &sz) {
    strm >> sz.width >> sz.height ;
    return strm ;
}

int main(int argc, char *argv[]) {

    ArgumentParser args ;

    string data_folder, output_file ;
    bool compute_markers = false ;
    cv::Size pattern_grid(4, 5) ;
    float pattern_width = 0.04, pattern_gap = 0.01 ;
    bool print_help = false, refine = false ;

    args.addOption("-h|--help", print_help, true).setMaxArgs(0).setDescription("print this help message") ;
    args.addOption("--data", data_folder).required().setName("<folder>").setDescription("Folder with captured images") ;
    args.addOption("--markers", compute_markers, true).setMaxArgs(0).setDescription("Recompute markers positions on images") ;
    args.addOption("--refine", refine, true).setMaxArgs(0).setDescription("Refine calibration using bundle adjustment") ;
    args.addOption("--grid", pattern_grid).setMinArgs(2).setMaxArgs(2).setName("<gx> <gy>").setDescription("Number of tags in x and y dimensions") ;
    args.addOption("--width", pattern_width).setName("<arg>").setDescription("Width of the AprilTag square in meters") ;
    args.addOption("--gap", pattern_gap).setName("<arg>").setDescription("Gap between AprilTags in the grid in meters") ;
    args.addOption("--out", output_file).required().setName("<output>").setDescription("Path to output file to store the camera") ;

    if ( !args.parse(argc, (const char **)argv) || print_help ) {
        cout << "Usage: camera_intrinsics [options]" << endl ;
        cout << "Options:" << endl ;
        args.printOptions(std::cout) ;
        exit(1) ;
    }

    CameraCalibration calib ;
    CameraCalibration::Data cdata ;

    if ( compute_markers ) {
        auto files = Path::entries(data_folder, matchFilesWithGlobPattern("*.png"), false) ;

        AprilTagDetector adet(AprilTag36h11, 4) ; // we need decimation othwerwise detection does not work

        AprilTagGridPattern agrid(pattern_grid, pattern_width, pattern_gap, adet) ;

        calib.detect(files, agrid, cdata) ;

        cdata.save(data_folder + "/calib.data") ;
    }
    else
        cdata.load(data_folder + "/calib.data") ;

    PinholeCamera cam ;

    vector<Matrix4d> extrinsics ;
    calib.run(cdata, cam, extrinsics, CV_CALIB_FIX_PRINCIPAL_POINT) ;
    if ( refine )
        calib.refine(cdata, cam, extrinsics, true) ;
    cam.write(output_file) ;
}
